
//Create a standard server using Node
//1. Identify the ingredients / components needed in order to perform the task
//http --> need to create a standard server --serve as foundation of the server that will be created
//will allow Node JS to establish a connection in order to transfer data using the Hyper Text Transfer Protocol

//how to use http
//require() --> this will allow us to acquire the resources needed to perform task
let http = require('http');

//2.Create a server using methods that we will take from the http resources
//http module contains the createServer() method that will allow us to create a standard server setup
//3. Identify the procedure / task that the server will perform once the connection has been established
	//identify the communication between the client and the server
// --> Request  -client to server
// -->Response  -server to client

http.createServer(function(request, response){
	response.end('Welcome to the Server, hello hello  nooo');
}).listen(3000)

//4.Select an address/location where the connection will be established or hosted

//5. Create a response in the terminal to make sure thta the server is running successfully
console.log('Server is running successfully');


//NPM
//6. Initialize a Node Package manager in NPM in our project

//7. Install your first package / dependency from the NPM that will allow you to make certain tasks a lot easier
	

	//install nodemon globally --> npm install -g nodemon

	// Uninstall
		//npm uninstall nodemon

	//multiple installations	

	//*1. nodemon
	//*2. express
	//syntax1: npm install <list of dependencies>
	//syntax2: npm i nodemon express


//8.create a remote repository to backup the project
//NOTE: node modules should not be included in the file structure that will be saved online
//**node modules should remain locally --not in the cloud
	//1.if saved in remote repo: it's going to take a longer time to stage / push your projects online
	//2. the node modules will cause error upon deployment(host online for use by clients)

	//USE .gitignore module  => allows us to modify the components or contents that version control will track